Saludemos la patria orgullosos
De hijos suyos podernos llamar
Y juremos la vida animosos,
Sin descanso a su bien consagrar.  

De la paz en la dicha suprema,
Siempre noble soñó El Salvador;
Fue obtenerla su eterno problema,
Conservarla es su gloria mayor.
Y con fe inquebrantable el camino
Del progreso se afana en seguir
Por llenar su grandioso destino,
Conquistarse un feliz porvenir.
Le protege una férrea barrera
Contra el choque de ruin deslealtad,
Desde el día que en su alta bandera
Con su sangre escribió: ¡LIBERTAD!
Escribió: ¡LIBERTAD!

Saludemos la patria orgullosos
De hijos suyos podernos llamar
Y juremos la vida animosos,
Sin descanso a su bien consagrar.  

Libertad es su dogma, es su guía
Que mil veces logró defender;
Y otras tantas, de audaz tiranía
Rechazar el odioso poder.
Dolorosa y sangrienta es su historia,
Pero excelsa y brillante a la vez;
Manantial de legítima gloria,
Gran lección de espartana altivez.
No desmaya en su innata bravura,
En cada hombre hay un héroe inmortal
Que sabrá mantenerse a la altura
De su antiguo valor proverbial.

Saludemos la patria orgullosos
De hijos suyos podernos llamar
Y juremos la vida animosos,
Sin descanso a su bien consagrar.  

Todos son abnegados, y fieles
Al prestigio del bélico ardor
Con que siempre segaron laureles
De la patria salvando el honor.
Respetar los derechos extraños
Y apoyarse en la recta razón
Es para ella, sin torpes amaños
Su invariable, más firme ambición.
Y en seguir esta línea se aferra
Dedicando su esfuerzo tenaz,
En hacer cruda guerra a la guerra:
Su ventura se encuentra en la paz.
