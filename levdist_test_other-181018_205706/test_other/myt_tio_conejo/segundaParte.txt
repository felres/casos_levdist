Tía Venada se chilló y quería hablar de otra cosa, pero el muy zángano se puso a echarle pullitas, y por aquí y por allá, hasta que la otra dijo que sí, y que ya tenían plazo para casarse.

“¡Hum! ¡Mala la chicha!”, pensó tío Conejo, y se puso a decir:

—Mire, tía Venada. ¿Usted es tontica de la cabeza o es que se hace? Quién dispone irse a casar con ese naguas miadas de tío Tigre... Si ese es un mamita de quien yo hago lo que me da mi regalada gana. Con decirle que a veces hasta de caballo me sirve.

—Eso sí que no puede ser.
