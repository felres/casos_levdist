There's no pictures on the wall
There's no rise and there's no fall
And in the morning I'm all right alone without you
There's no phones and there's no calls
There's no talking to you at all
And I don't care late at night when it's not you

But when I look out in the rain
I think about the past, I want it again
I think about the way you feel inside
I start losing my mind, I start losing my mind, my mind, my mind

There's no formulated plans
There's no way and there's no chance
It's been too long and it's full all back behind me
There's no going down that road
And I know right where it goes
If I keep walking, away nothing reminds me

But when I look out in the rain
I think about the past, I want it again
I think about the way you feel inside
I start losing my mind, I start losing my mind, my mind, my mind

[Instrumental]

There's no fights and there's no tears
There's no need if you're not here
But I'm not the same anyyyyway anyyymore
There's no need to write this song
Cause there's no changing what's been done
And there's no changing what's inside
And it looks like it's gonna storm

I look out in the rain
I think about the past, I want it again
I think about the way you feel inside
I'm losing my mind, I'm losing my, losing my, losing all the way

I look out in the rain
I think about the past, I want it again
I think about the way you feel inside
I'm losing my mind, I'm losing my mindd
 
