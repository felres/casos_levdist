Once a royal member of Dragon School, he is now a scholar of the soul arts. He turned Undead over a hundred years ago, and has been roaming Lordran, searching for more wisdom, ever since. He is first encountered in Sen's Fortress, trapped inside a hanging cage in a blocked off area, and will teach relatively advanced magic after being rescued. He had a traveling companion in Griggs of Vinheim, but became separated from his company upon entering Lordran. Griggs mentions that Logan did this "For his safety...".
 
Encounters

1. Found inside a cage in Sen's Fortress

    To reach him you will have to rotate the "boulder machine," so the boulder crushes a wall behind a sleeping guard or wake up the sleeping guard and force him to attack the wall; it can break it with its Estoc attack.
    You can use the Cage Key or the Master Key to open the cage. The Cage Key is found at the top of the castle. Jump across the broken walkway that leads to a lone tower, then descend the stairs. Kill the guard and pick up the key.
    It is not necessary to have freed Griggs of Vinheim.

2. Back at the Firelink Shrine

    Logan will teach you various advanced magic spells, but only if your intelligence stat is 15 or higher.

3. Big jail cell in The Duke's Archives

    Later in the game, when the golden fog doors have been opened, you encounter him in a here. Attack Seath the Scaleless in his room, he is invincible and you will be cursed or killed (perfect use for rare ring of sacrifice). Upon revival you wake up next to the 2nd bonfire of the Archives, trapped inside a cell. Kill the snake guard through the bars to grab the key to your cell off of his corpse. From the bonfire, turn right out of the cell and head all the way down the spiral stairs to find Logan in a large cage at the bottom guarded by a group of Pisacas (ie. the blue snake-like enemies with octopus heads)
    The Archive Tower Giant Cell Key which opens his prison can be found in a chest in the room with the collapsible staircase on the bottom level of the main building (the room before the crystal golem garden that leads to Crystal Cave).
    Either after you have rescued him from his cell or after you have defeated Seath the Scaleless, you can find him behind the shortcut bookcase that you opened to gain access to the crystal golem garden. Pass through it and he is on a small alcove in the left of the library.
    At this point he will sell you crystal variants of the original spells along with the originals. You can also talk to him for hints on how to defeat Seath the Scaleless. Note that you MUST buy all of his spells or he will not become insane.
    After you buy all of his spells and defeat Seath the Scaleless, Logan will not recognize you and tell you not to bother him, which leads to triggering his final appearance.
    Talk to him several times, and you'll notice that he's clearly gone mad. Now rezone / reload again (may take two reloads) and he'll have left that spot.

4. Final: Original room from Seath the Scaleless.

    ‍His final appearance is in the original room where you first encountered Seath. Logan has gone insane and starts attacking you with his spells. You will have to resort to killing him. For this you will get his drops.
    After you kill him, go back to the room where he sold you his spells. You will find a chest in his place with Logan's Catalyst, as well as the remainder of the Big Hat's Set.

